PRODUCT_PACKAGES += \
    OnePlusCamera \
    OnePlusCameraService

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/system/etc/permissions/com.oneplus.camera.service.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.oneplus.camera.service.xml \
    $(LOCAL_PATH)/system/etc/sysconfig/hiddenapi-package-whitelist-op.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/hiddenapi-package-whitelist-op.xml \
    $(LOCAL_PATH)/prebuilt/apps/lib/arm64/libsnpe_dsp_v66_domains_v2_skel.so:$(TARGET_COPY_OUT_SYSTEM_EXT)/priv-app/OnePlusCamera/lib/arm64/libsnpe_dsp_v66_domains_v2_skel.so
